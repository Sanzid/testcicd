# CI/CD
本ドキュメントは、タグノバOTAシステムのCI/CDに関わるデプロイ手順を記す。本デプロイでは、(1)CodeCommit Repository、(2)CodeBuild Build project、(3)EventBridge Rule、(4)LambdaFunction、(5)ECR Repositoryの5つのAWS Serviceをデプロイし、各サービス実行に必要となるアクセス権限を付与するIAM role & IAM policyも合わせて作成・付与する。デプロイ手順は大きく次の3つの手順から構成される。  

***手順 1. AWS Service の Deploy***  
***手順 2. gitlab の Mirroring設定***  
***手順 3. gitlab repository に Build 関連ファイルの準備***  

## 手順 1. AWS Service の Deploy
AWS Service の Deploy は、AWS SAM CLI (Serverless Application Model)にて実行する。
本手順において、Cloudformation の [template.yaml](./template.yaml) に記載のAWS Service Stack を展開する.

初めてのデプロイの場合、`--guided`オプションを利用し対話形式でデプロイ設定ファイル`samconfig.toml`を作成しつつ、デプロイを実施する。※ 次回以降は、`--guided`オプション不要。

```
$ cd tmc-ota-deliverables/cicd
$ sam deploy --guided

Configuring SAM deploy
======================

Looking for config file [samconfig.toml] :  Not found

Setting default arguments for 'sam deploy'
=========================================
Stack Name [sam-app]: TugnovaCICD
    <-- cloudformation の stack名となります. 適切な名称を設定下さい.

AWS Region [ap-northeast-1]: 
    <-- 適切なRegionを設定下さい.

Parameter ApplicationName [ros-sample-app]: 
    <-- CodeCommit & GGv2 Component Nameへ反映. 適切な名称を設定下さい.

Parameter RosAppS3Bucket [ros-app-bucket]: 
    <-- ROS App の関連ファイルを格納する S3 Bucket 名を入力下さい.

Parameter CDFTokenExchangeRoleName [GreengrassTokenExchangeRole]: 
    <-- CDFにてinstallされた、TokenExchangeRoleの名前を入力下さい.
        -- Stack [cdf-greengrass2-installer-config-generators-<Env>] ->
        -- Outputs -> Key: TokenExchangeRoleName の "Value"を参照

    ↓ 以下項目については、default値で問題ございません。
#Shows you resources changes to be deployed and require a 'Y' to initiate deploy
Confirm changes before deploy [y/N]: y
    <-- Deploy 時に変更を確認する場合 y を選択

#SAM needs permission to be able to create roles to connect to the resources in your template
Allow SAM CLI IAM role creation [Y/n]: 
#Preserves the state of previously provisioned resources when an operation fails
Disable rollback [y/N]: 
Save arguments to configuration file [Y/n]: 
SAM configuration file [samconfig.toml]: 
SAM configuration environment [default]: 
```

Deploy 時の変更確認を y とした場合、以下にて y を選択することで、Deploy実施

```
Previewing CloudFormation changeset before deployment
======================================================
Deploy this changeset? [y/N]: y
```

Deploy が成功すると、以下メッセージが表示

```
Successfully created/updated stack - TugnovaCICD in ap-northeast-1
```

## 手順 2. gitlab の Mirroring設定
gitlab 内のコードをミラーリングし、CodeCommitへコピーする設定を行う。CodeCommitへコピーすることで、CodeBuildによるBuildを実行し、Docker ContainerのECRへの登録、GGv2 Componentの作成を自動化する。

手順 1.にて`ApplicationName`で指定したCodeCommit Repositoryが作成されおり、このRepositoryへ Mirroring する設定を行う。

1. CodeCommitへアクセス（ミラーリング）するため、アクセス権限を持つIAMユーザを作成
    1. CodeCommitへのアクセスを可能とするIAMポリシーを作成し登録  
        以下、JSONをファイル `gitlab-mirroring-policy.json` に保存する
        ```JSON file
        {
            "Version": "2012-10-17",
            "Statement" : [
                {
                "Effect" : "Allow",
                "Action" : [
                    "codecommit:GitPull",
                    "codecommit:GitPush"
                ],
                "Resource" : "arn:aws:codecommit:ap-northeast-1:<AWS ACCOUNT>:<ApplicationName>"
                }
            ]
        }
        ```
        IAMポリシーの登録
        ```
        $ aws iam create-policy --policy-name gitlabMirroringPolicy --policy-document file://gitlab-mirroring-policy.json

        {
            "Policy": {
                "PolicyName": "gitlab-mirroring-policy",
        ……
                "Arn": "arn:aws:iam::<AWS ACCOUNT>:policy/gitlabMirroringPolicy",
        ……
            }
        }
        ```
    2. MirroringするためのIAMユーザを新規作成し、アクセスポリシーを割り当てる
        ```
        $ aws iam create-user --user-name <USER NAME>
        $ aws iam attach-user-policy --user-name <USER NAME> --policy-arn arn:aws:iam::<AWS ACCOUNT>:policy/gitlabMirroringPolicy
        ```
    3. gitlabのmirroring時に必要となるcredentialを生成する
        ```
        $ aws iam create-service-specific-credential --user-name <USER NAME> --service-name codecommit.amazonaws.com

        {
            "ServiceSpecificCredential": {
            ……
                "ServiceName": "codecommit.amazonaws.com",
                "ServiceUserName": "xxxxxxxxxxxx",
                "ServicePassword": "xxxxxxxxxxxx",
            ……
            }
        }
        ```
        上記の、`ServiceUserName`と`ServicePassword`を使い、gitlab の Mirroring 設定を行う.

2. gitlab の repository で、 CodeCommitへのミラーリングを設定する。
    1. gitlab のリポジトリのメニューから「Settings」→ 「Respository」を選択
        ![RepositorySetting](images/2.1.RepositorySetting.png)
    2. 「Mirroring repositories」の「Expand」を選択
        ![MirroringRepository](images/2.2.MirroringRepository.png)
    3. ミラーリングに以下の必要な情報を入力し、「Mirror repository」をクリック
        - Git repository URL: https://<`ServiceUserName`>@git-codecommit.ap-northeast-1.amazonaws.com/v1/repos/<`ApplicationName`>
        - Mirror direction: Push
        - Authentication method: Password
        - Password: <`ServicePassword`>     ※ 1.3.で作成

        ![MirroringRepository](images/2.3.MirroringSetting.png)

3. Mirroring の動作確認  
    gitlab のミラーリング設定画面で、同期をクリック
    ![MirroringSync](images/3.MirroringSync.png)

    CodeCommit Repository にて、コード類がコピーされていることを確認

## 手順 3. gitlab repository に Build 関連ファイルの準備
CodeBuild においてビルド方法を示す`buildspec.yml`と、GreegrassV2 Component のRecipeの雛形となる`recipe_template.yaml`を、repository の Top Directory `config/` 以下に配備する.

    gitlab Repository of ROS application
    │──config/                       : App Name (GGv2 Component name)
    │  │──buildspec.yml              : CodeBuild Configuration file
    │  └──recipe_template.yaml       : GGv2 Recipe template file
※ 上記ファイルのリンクは、サンプル提供しているROSアプリで作成した関連ファイルである。

なお、buildspec.yml 内にて利用可能な環境変数の一部を以下に記す. 詳細は[公式Doc](https://docs.aws.amazon.com/ja_jp/codebuild/latest/userguide/build-env-ref-env-vars.html)を参照.
- APPLICATION_NAME: 手順 1.2.で設定したParameter `ApplicationName`
- S3_BUCKET_NAME: 手順 1.2.で設定したParameter `RosAppS3Bucket`
- AWS_REGION: CodeBuildを実行しているAWS region (ex. ap-northeast-1)

※ 上記の環境変数 (APPLICATION_NAME, S3_BUCKET_NAME)は、CloudFormation の[Template](./template.yaml)にて設定.
```
Parameters:
  ApplicationName: # Stack作成時に値を設定します。
    Description: Application Name for CodeBuild
    Default: ros-sample-app
    Type: String
~~~~~

Resources:
~~~~~
  RosBuildProject:
    Type: AWS::CodeBuild::Project
    Properties:
~~~~~
      Environment:
~~~~~
        EnvironmentVariables:
          - Name: APPLICATION_NAME  # こちらで Codebuild の環境変数に設定します。
            Type: PLAINTEXT
            Value: !Ref ApplicationName
          - Name: S3_BUCKET_NAME
            Type: PLAINTEXT
            Value: !Ref RosAppS3Bucket
```