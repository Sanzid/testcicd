import json
import boto3

def triggerCodeBuildProject(repository_name, tag):
    print(f"triggerCodeBuildProject is called. repository_name: {repository_name} & tag: {tag}")

    try:
        # Creating boto3 codebuild
        client = boto3.client('codebuild')

        # identify the CodeBuild Project
        response = client.list_projects()
        projects = response['projects']
        response = client.batch_get_projects(names=projects)

    except Exception as e:
        print("Failed to get build projects. Error: " + str(e))
        exit(e)

    buildIds = []
    
    for project in response.get('projects'):
        
        if 'location' in project.get('source') and repository_name in project['source']['location']:
            try:
                # Kick the CodeBuild Project
                response = client.start_build(projectName=project.get('name'), sourceVersion=tag)
                print(f"CodeBuild is invoked. buildId: {response['build']['id']}")
            except Exception as e:
                print("Failed to start build project. Error: " + str(e))
                continue

            buildIds.append(response['build']['id'])
            
    if len(buildIds) == 0:
        print("Cannot find an appropriate CodeBuild project.")
        return
    
    return buildIds

def lambda_handler(event, context):
    """Sample Lambda function reacting to EventBridge events

    Parameters
    ----------
    event: dict, required
        Event Bridge Events Format

        Event doc: https://docs.aws.amazon.com/eventbridge/latest/userguide/event-types.html

    context: object, required
        Lambda Context runtime methods and attributes

        Context doc: https://docs.aws.amazon.com/lambda/latest/dg/python-context-object.html

    Returns
    ------
        Build Ids created by CodeBuild, which are invoked by this function
    """
    print("CodeCommitDetectNewVersion is called")
    print(json.dumps(event))
    
    ret = []
    if 'event' in event.get('detail') and event['detail']['event'] == "referenceCreated":
        # An event with a new tag attached to the repository of CodeCommit 
        ret = triggerCodeBuildProject(event['detail'].get('repositoryName'), event['detail'].get('referenceFullName'))

    return ret