import argparse
import json
from hashlib import new
import boto3

def calc_etag(inputfile):
    
    m = new('md5', usedforsecurity=False)
    with open(inputfile, "rb") as f:
        read = f.read()
        m.update(read)

    return m.hexdigest()

def upload_file(upfile, s3uri, versionId=None, backupS3Uri=None):

    upload_bucket = s3uri.split('/')[2]
    upload_key = '/'.join(s3uri.split('/')[3:])

    try:
        print("Creating boto3 S3 resource...")
        s3 = boto3.resource('s3')
        print("Successfully created boto3 S3 resource")
    except Exception as e:
        print("Failed to create boto3 s3 resource. Error: " + str(e))
        exit(e)
        
    try:
        print("Getting e_tag of the object from S3 bucket ...")
        if versionId == None:
            s3e_tag = s3.ObjectSummary(upload_bucket, upload_key).e_tag.replace('"','')
        else:
            s3e_tag = s3.ObjectVersion(upload_bucket, upload_key, versionId).head()['ETag'].replace('"','')
        print("Successfully got e_tag info.")
    except Exception as e:
        print("Failed to get e_tag info. Error: " + str(e))
        s3e_tag = ''

    if calc_etag(upfile) != s3e_tag:
        print("The data has changed locally. Uploading the file to S3 for backup ...")

        if backupS3Uri != None:
            upload_bucket = backupS3Uri.split('/')[2]
            upload_key = '/'.join(backupS3Uri.split('/')[3:])

        try:
            # Upload data for backup
            s3.meta.client.upload_file(upfile, upload_bucket, upload_key)
        except Exception as e:
            print("Failed to upload the local file. Error: " + str(e))
            exit(e)
        print("Successfully uploaded to S3")
        
    else:
        print("The data has not changed locally. There is no need for backup.")
   
if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='File handler program to upload related files to S3')
    parser.add_argument('config', help='Configuration of the related files in JSON format')
    parser.add_argument('-d', '--directory', default='/root/resources', help='The local directory path where the related files are stored')
    args = parser.parse_args()

    directory = args.directory
    confRelatedFiles = json.loads(args.config)

    for key in confRelatedFiles:
        if confRelatedFiles[key].get('cloudBackup') == True:

            upfile = '{0}/{1}'.format(directory, confRelatedFiles[key]['s3Uri'].split('/')[-1])

            print("Back up {0} file to the S3".format(confRelatedFiles[key]['s3Uri'].split('/')[-1]))

            upload_file(upfile, confRelatedFiles[key]['s3Uri'], confRelatedFiles[key].get('versionId'), confRelatedFiles[key].get('backupS3Uri'))
