AWS_ACCOUNT=138795747269
AWS_RESIONG="ap-northeast-1"
ECR_REPOSITORY="file-handler-test"

aws ecr get-login-password --region ${AWS_RESIONG} | docker login --username AWS --password-stdin ${AWS_ACCOUNT}.dkr.ecr.${AWS_RESIONG}.amazonaws.com
docker build -t file-handler-test .
docker tag file-handler-test:latest ${AWS_ACCOUNT}.dkr.ecr.${AWS_RESIONG}.amazonaws.com/${ECR_REPOSITORY}:latest
docker push ${AWS_ACCOUNT}.dkr.ecr.${AWS_RESIONG}.amazonaws.com/${ECR_REPOSITORY}:latest
