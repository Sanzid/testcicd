# File handler Program
本ファイルは、プログラム実行に必要な関連ファイルをS3からダウンロード / アップロードするプログラムである。GGv2 component内にDeployし、component記載のConfigurationを読み取り、DL/ULする事を目的とする。  

本プログラムは、Greengrassの機能を補完するために作成したものであり、Greengrass 単体では上手く扱えない以下の課題に対応したものである。  

**[ 課題 ]**  
greengrass では、ファイルを配信する方法として、`Manifests -> Artifacts` 内にURIを指定することで、greengrass device へとファイルを配信することができる。しかし、デバイス毎に異なるファイル（地図ファイル等の関連ファイル）を配信するような仕様となっていない。具体的には、デバイス毎に固有のファイルを配信するためには、recipe をデバイス毎に作成し　`Manifests -> Artifacts`　内にデバイス固有のファイルURIを記述し、greengrass component の異なるVersionとして登録する必要がある。このような方法では、デバイス毎、関連ファイルが変更される度に新しい greengrass component version を作成する必要があり、管理が煩雑になることが課題となる。

本課題を解決するため、greengrass device 毎に変更可能な "Configuration" 領域を活用し、必要なファイルのURIを "Configuration" 領域に記載し、greengrass device へと通知する方法を採用した。この方法では、"Configuration" 領域に記載されたファイルのURIから、ファイルをダウンロードするプログラム（greengrass device上で動作する）が必要なため、そのサンプルプログラムを提供するものである。

## プログラムの構成
本プログラムは、download.py、upload.pyの2種類からなり、それぞれS3からのファイルダウンロード、S3へのファイルアップロードを行う。両プログラム共、引数として`<config>`を指定し実行する。
```
python3 download.py <config>
python3 upload.py <config>
```
`<config>`の書式はJSON形式であり、サンプルは以下の通りである。
```
{
    "relatedFiles": {
        "sampleConfig.txt": {
            "s3Uri": "s3://<YOUR BUCKET>/<PATH TO THE FILE>",
            "versionId": "<S3 VERSION_ID>",
            "cloudBackup": true
        },
        "sampleGroupConfig.txt": {
            "s3Uri": "s3://<YOUR BUCKET>/<PATH TO THE FILE>",
            "versionId": "<S3 VERSION_ID>",
            "cloudBackup": true,
            "backupS3Uri": "<YOUR BUCKET>/<PATH WHERE BACK UP THE FILE>"
        }
    }
}
```
relatedFiles内に必要な関連ファイルを入力していく形式であり、本サンプルでは(1)`sampleConfig.txt`と(2)`sampleGroupConfig.txt`の2つのファイルがある場合を記す。各ファイルには、以下の要素が含まれる。
- s3Uri: ダウンロードするファイルを格納しているS3のURIを指定 **[REQUIRED]**
- versionId: s3ファイルの特定のVersionを指定する場合に利用  
  指定が無い場合、default値は最新(Current)Versionとなる
- cloudBackup: タグノバ機にて変更・更新した場合、cloudへuploadしbackupするか否かを指定する  
  default 値は false (backupしない)である。
- backupS3Uri: cloudBackup が true の場合で、uploadするS3のURIを指定する場合に利用
  default 値は `s3Uri`であり、異なる場所にアップロードしたい場合に追加で記載する

また、GGv2 Component の Recipe内の Configに記述する場合、以下のように設定する。(yaml形式)
```
ComponentConfiguration:
  DefaultConfiguration:
    relatedFiles:
      sampleConfig.txt:
        s3Uri: s3://<YOUR BUCKET>/<PATH TO THE FILE>
        versionId: <S3 VERSION_ID>
        cloudBackup: true
      sampleGroupConfig.txt:
        s3Uri: s3://<YOUR BUCKET>/<PATH TO THE FILE>
        versionId: <S3 VERSION_ID>
        cloudBackup: true
        backupS3Uri: s3://<YOUR BUCKET>/<PATH WHERE BACK UP THE FILE>
```
※ Recipe内において、本Config情報は`{configuration:/relatedFiles}`で参照可能.  

その他のオプション引数として、`-d DIRECTORY`がある. 本オプションを用いることで、Download/Uploadするファイルを保存するローカルディレクトリを指定することができる. なお、本オプションを指定しない場合、default値である`/root/resources`が採用され、フォルダ `/root/resources`配下に格納ファイルを保存する.

※ `python3 download.py -h`,  `python3 upload.py -h`も参照のこと.

## GGv2 Component での利用方法
デプロイ方法は、(1)docker container、(2)python program単体、の２つの方法を用意した。(1)では、上記Python Program 及び実行環境を含んだコンテナイメージを作成し、コンテナとして GGv2 core node へとデプロイする。(2)は、Python program 単体をGGv2 core node へとデプロイするものであり、GGv2 core node上にて、boto3 library の install 含む実行環境を構築する方法である。以下、それぞれの方法について記載する。

### 1. docker container によるデプロイ
1. コンテナレポジトリ(ECR repository)の作成  
    ```
    aws ecr create-repository \
    --repository-name file-handler \
    --image-scanning-configuration scanOnPush=true \
    --region ap-northeast-1
    ```
2. コンテナのビルド & コンテナレポジトリへアップロード  
    `pushContainerImage.sh`を実行することで、(1)コンテナのビルド、(2)ステップ1で作成したレポジトリへのアップロードを実施する。Script実行に先立ち、ファイル内の以下の変数を設定する事。
    - AWS Account ID の設定
    - AWS Region の設定
    - 手順1)で作成した、ECR repository Nameの設定
    ```
    $ ./pushContainerImage.sh
    ```

3. GGv2 Component Recipe 内で download / upload の記述  
    Recipe の サンプルは、[recipe_docker.yaml](./recipe_docker.yaml)を参照.
    - GGv2 Component で (1)Container実行するためにDockerApplicationManagerを、(2)S3へアクセスするためにTokenExchangeServiceが必要となるため、Dependencyに以下を追加すること。
      ```
      ComponentDependencies:
        aws.greengrass.DockerApplicationManager:
          VersionRequirement: ~2.0.0
        aws.greengrass.TokenExchangeService:
          VersionRequirement: ^2.0.0
            DependencyType: HARD
      ```
    - Container Image をOTAするため、Artifactにて、ECR repositoryを指定すること
      ```
      Artifacts:
        - URI: "docker:<YOUR AWS ACCOUNT>.dkr.ecr.ap-northeast-1.amazonaws.com/file-handler:latest"
      ```
    - Container 内のプログラム実行時には、(1)S3へアクセスするためクレデンシャルを取得するため、環境変数を渡し、ホストネットワークに接続し、(2)ホスト側Volumeへアクセスするため、ホストOS側のボリュームをmountし実行する。
      ```
      docker run --env AWS_CONTAINER_AUTHORIZATION_TOKEN --env AWS_CONTAINER_CREDENTIALS_FULL_URI --net=host --mount type=bind,src={work:path}/resources,dst=/root/resources file-handler:latest python3 download.py '{configuration:/relatedFiles}'
      ```
以上の手順により、GGv2 Component において、file handlerの実行が可能となる。  

以下、docker container の python プログラム実行オプションについて説明する.  
- 環境変数にて指定している `--env AWS_CONTAINER_AUTHORIZATION_TOKEN --env AWS_CONTAINER_CREDENTIALS_FULL_URI` は、AWS Service へアクセスするために必要となる認証トークンと、 クレデンシャルへのアクセス情報をコンテナ内へと渡している (認証トークン & クレデンシャル情報は、TokenExchangeService で提供される)。
- 上記クレデンシャルへのアクセス情報を用いて、実際のクレデンシャルを取得するため、コンテナのネットワーク接続設定を `--net=host` としている。
- コンテナ内でダウンロードしたファイルを、ホスト側からもアクセスできるようボリュームのマウント設定を、`--mount type=bind,src={work:path}/resources,dst=/root/resources` としている。これは、GGv2 device 上のディレクトリ`{work:path}/resources` をコンテナ内のディレクトリ `/root/resources` へマッピングすることを意味する。なお、{work:path}は、recipe内で利用可能な環境変数であり、プログラムを実行する work directory を指す。

### 2. python program 単体によるデプロイ
1. file handler program (download.py & upload.py) を S3 Bucket へ Upload
    ```
    $ aws s3 cp download.py s3://<YOUR BUCKET>/<PATH TO download.py>
    $ aws s3 cp upload.py s3://<YOUR BUCKET>/<PATH TO upload.py>
    ```
2. GGv2 Component Recipe 内で download / upload の記述  
    Recipe の サンプルは、[recipe_python.yaml](./recipe_python.yaml)を参照.
    - Python Program をOTAするため、Artifactにて、S3 URIを指定すること
      ```
      Artifacts:
        - URI: "S3://<YOUR BUCKET>/<PATH TO download.py>"
        - URI: "S3://<YOUR BUCKET>/<PATH TO upload.py>"
      ```
    - Python Program 実行に必要な libraryを Lifecycle/Install にて インストールする
      ```
      python3 -m pip install --upgrade pip && python3 -m pip install boto3
      ```
    - Python Program を実行する
      ```
      python3 -u {artifacts:path}/download.py '{configuration:/relatedFiles}' -d {work:path}/resources
      python3 -u {artifacts:path}/upload.py '{configuration:/relatedFiles}' -d {work:path}/resources
      ```
以上の手順により、GGv2 Component において、file handlerの実行が可能となる。  