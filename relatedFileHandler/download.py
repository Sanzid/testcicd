import argparse
import json
import boto3

def download_file(s3uri, filepath, versionId=None):

    download_bucket = s3uri.split('/')[2]
    download_key = '/'.join(s3uri.split('/')[3:])

    try:
        print("Creating boto3 S3 resource...")
        s3 = boto3.resource('s3')
        bucket = s3.Bucket(download_bucket)
        print("Successfully created boto3 S3 resource")

    except Exception as e:
        print("Failed to create boto3 s3 resource. Error: " + str(e))
        exit(e)

    try:

        if versionId == None:
            print("Downloading file (latest version)...")
            bucket.download_file(download_key, filepath)
        else:
            print("Downloading file (versionId: {})...".format(versionId))
            bucket.download_file(download_key, filepath, ExtraArgs={'VersionId': versionId})

        print("Successfully downloaded S3 object")

    except Exception as e:
        print("Failed to download S3 object. Error: " + str(e))
        exit(e)


def download_multiple_files(s3uri, directory):

    download_bucket = s3uri.split('/')[2]
    download_folder = '/'.join(s3uri.split('/')[3:])

    try:
        print("Creating boto3 S3 resource...")
        s3 = boto3.resource('s3')
        bucket = s3.Bucket(download_bucket)
        print("Successfully created boto3 S3 resource")

    except Exception as e:
        print("Failed to create boto3 s3 resource. Error: " + str(e))
        exit(e)

    download_targets = []
    # search objects within the folder
    for obj in bucket.objects.filter(Prefix=download_folder):
        download_targets.append(obj.key)

    if not download_targets:
        # the uri is an object uri, not folder
        print("The file to be downloaded could not be found.")
        return

    try:
        print("Downloading targets files...")

        for obj in download_targets:
            bucket.download_file(obj, '{}/{}'.format(directory,obj.split('/')[-1]))

        print("Successfully downloaded S3 objects")
    except Exception as e:
        print("Failed to download S3 object. Error: " + str(e))
        exit(e)

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='File handler program to download related files from S3')
    parser.add_argument('config', help='Configuration of the related files in JSON format')
    parser.add_argument('-d', '--directory', default='/root/resources', help='The local directory path to store the related files')
    args = parser.parse_args()

    directory = args.directory
    confRelatedFiles = json.loads(args.config)

    for key in confRelatedFiles:
        filepath = '{0}/{1}'.format(directory, confRelatedFiles[key]['s3Uri'].split('/')[-1])
        print(filepath)
        if 'versionId' in confRelatedFiles[key]:
            download_file(confRelatedFiles[key]['s3Uri'], filepath, confRelatedFiles[key]['versionId'])
        else:
            download_file(confRelatedFiles[key]['s3Uri'], filepath)
 
