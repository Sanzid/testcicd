AWSTemplateFormatVersion: '2010-09-09'
Transform: AWS::Serverless-2016-10-31
Description: >
  codePipelineForCommitTag

  Sample SAM Template for codePipelineForCommitTag

# More about Globals: https://github.com/awslabs/serverless-application-model/blob/master/docs/globals.rst
Globals:
  Function:
    Timeout: 3

Parameters:
  ApplicationName:
    Description: Application Name for CodeBuild
    Default: ros-sample-app
    Type: String

  RosAppS3Bucket:
    Description: S3 Bucket Name for storing ros application files
    Type: String
    Default: ros-app-bucket

  CDFTokenExchangeRoleName:
    Description: TokenExchangeRoleName created by CDF stack 'cdf-greengrass2-installer-config-generators'
    Type: String
    Default: GreengrassTokenExchangeRole

Resources:
  CodeCommit:
    Type: AWS::CodeCommit::Repository
    Properties:
      RepositoryName: !Ref ApplicationName
      RepositoryDescription: Git Repository for ROS2 Sample Program

  ECRRepository:
    Type: AWS::ECR::Repository
    Properties: 
      RepositoryName: !Ref ApplicationName
      ImageScanningConfiguration:
        ScanOnPush: true
    DeletionPolicy: Delete

  CodeBuildServiceRole:
    Type: AWS::IAM::Role
    Properties:
      Path: /
      AssumeRolePolicyDocument:
        Statement:
          - Effect: Allow
            Principal:
              Service:
                - codebuild.amazonaws.com
            Action:
                - sts:AssumeRole
      Policies:
        - PolicyName: !Sub CodeBuildPolicy-${ApplicationName}
          PolicyDocument:
            Version: 2012-10-17
            Statement:
              - Effect: Allow
                Resource:
                  - !Sub arn:aws:logs:${AWS::Region}:${AWS::AccountId}:log-group:/aws/codebuild/${ApplicationName}
                  - !Sub arn:aws:logs:${AWS::Region}:${AWS::AccountId}:log-group:/aws/codebuild/${ApplicationName}:*
                Action:
                  - logs:CreateLogGroup
                  - logs:CreateLogStream
                  - logs:PutLogEvents
              - Effect: Allow
                Resource:
                  - !Sub arn:aws:codecommit:${AWS::Region}:${AWS::AccountId}:${ApplicationName}
                Action:
                  - codecommit:GitPull
              # Permission required when logging in to upload images to ECR
              - Effect: Allow
                Resource:
                  - "*"
                Action:
                  - ecr:GetAuthorizationToken
              - Effect: Allow
                Resource:
                  - !Sub arn:aws:ecr:${AWS::Region}:${AWS::AccountId}:repository/${ApplicationName}
                Action:
                  - ecr:CompleteLayerUpload
                  - ecr:UploadLayerPart
                  - ecr:InitiateLayerUpload
                  - ecr:BatchCheckLayerAvailability
                  - ecr:PutImage
              - Effect: Allow
                Resource:
                  - !Sub arn:aws:greengrass:${AWS::Region}:${AWS::AccountId}:components:${ApplicationName}
                Action:
                  - greengrass:CreateComponentVersion
              - Effect: Allow
                Resource:
                  - !Sub arn:aws:s3:::${RosAppS3Bucket}
                  - !Sub arn:aws:s3:::${RosAppS3Bucket}/*
                Action:
                  - s3:GetObject
                  - s3:DeleteObject
                  - s3:PutObject

  RosBuildProject:
    Type: AWS::CodeBuild::Project
    Properties:
      Name: !Ref ApplicationName
      Description: CodeBuild project assuming to build a docker container of ROS2
      ServiceRole: !Ref CodeBuildServiceRole
      Artifacts:
        Type: NO_ARTIFACTS
      Environment:
        Type: LINUX_CONTAINER
        ComputeType: BUILD_GENERAL1_SMALL
        Image: aws/codebuild/standard:5.0
        PrivilegedMode: true
        EnvironmentVariables:
          - Name: APPLICATION_NAME
            Type: PLAINTEXT
            Value: !Ref ApplicationName
          - Name: S3_BUCKET_NAME
            Type: PLAINTEXT
            Value: !Ref RosAppS3Bucket
      Source:
        Location: !Sub https://git-codecommit.${AWS::Region}.amazonaws.com/v1/repos/${ApplicationName}
        Type: CODECOMMIT
        BuildSpec: config/buildspec.yml
        GitCloneDepth: 1
      LogsConfig:
        CloudWatchLogs:
          Status: ENABLED

  InvokeCodeBuildFunction:
    Type: AWS::Serverless::Function # More info about Function Resource: https://github.com/awslabs/serverless-application-model/blob/master/versions/2016-10-31.md#awsserverlessfunction
    Properties:
      CodeUri: invoke_codebuild
      Handler: app.lambda_handler
      Runtime: python3.9
      Policies:
        - Statement:
          # Need access to all resources when searching for the corresponding codebuild project
          - Sid: listCodeBuildProjectsPolicy
            Effect: Allow
            Action:
              - codebuild:ListProjects
              - codebuild:BatchGetProjects
            Resource: 
              - "*"
          - Sid: invokeCodeBuildProjectsPolicy
            Effect: Allow
            Action:
              - codebuild:StartBuild
            Resource:
              - !Sub arn:aws:codebuild:${AWS::Region}:${AWS::AccountId}:project/${ApplicationName}
      Architectures:
        - x86_64
      Events:
        TagDetectOnCodeCommit:
          Type: CloudWatchEvent # More info about CloudWatchEvent Event Source: https://github.com/awslabs/serverless-application-model/blob/master/versions/2016-10-31.md#cloudwatchevent
          Properties:
            #EventBusName: your-event-bus-name #Uncomment this if your events are not on the 'default' event bus
            Pattern:
              source:
                - aws.codecommit
              detail-type:
                - CodeCommit Repository State Change
              resources:
                - !Sub arn:aws:codecommit:${AWS::Region}:${AWS::AccountId}:${ApplicationName}

  GGv2AccessArtifactPolicy:
    Type: AWS::IAM::Policy
    # Access policy to allow the GGv2 components to download artifacts required for deploy
    Properties:
      PolicyName: !Sub accessComponentArtifacts-${ApplicationName}
      Roles: 
        - !Ref CDFTokenExchangeRoleName
      PolicyDocument: 
        Version: 2012-10-17
        Statement:
          # Permission required when logging in to download images from ECR
          - Effect: Allow
            Resource:
              - "*"
            Action:
              - ecr:GetAuthorizationToken
          - Effect: Allow
            Resource:
              - !Sub arn:aws:ecr:${AWS::Region}:${AWS::AccountId}:repository/${ApplicationName}
              - !Sub arn:aws:ecr:${AWS::Region}:${AWS::AccountId}:repository/file-handler
            Action:
              - ecr:GetDownloadUrlForLayer
              - ecr:BatchGetImage
          - Effect: Allow
            Resource:
              - !Sub arn:aws:s3:::${RosAppS3Bucket}
              - !Sub arn:aws:s3:::${RosAppS3Bucket}/*
            Action:
              - s3:ListBucket
              - s3:PutObject
              - s3:GetObject
              - s3:GetObjectVersion
              - s3:GetObjectAttributes
              - s3:GetObjectVersionAttributes

Outputs:
  # ServerlessRestApi is an implicit API created out of Events key under Serverless::Function
  # Find out more about other implicit resources you can reference within SAM
  # https://github.com/awslabs/serverless-application-model/blob/master/docs/internals/generated_resources.rst#api
  InvokeCodeBuildFunction:
    Description: "Invoke CodeBuild Lambda Function ARN"
    Value: !GetAtt InvokeCodeBuildFunction.Arn
  InvokeCodeBuildFunctionIamRole:
    Description: "Implicit IAM Role created for Invoke CodeBuild function"
    Value: !GetAtt InvokeCodeBuildFunctionRole.Arn
